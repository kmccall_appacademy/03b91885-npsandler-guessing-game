# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  true_num = rand(100) + 1
  puts "Let's play a game.".center(50)
  puts "I'm going to think of a number".center(50)
  puts "and you get to guess what it is!".center(50)
  puts "guess a number"
  play(true_num, [])
end

def play(true_num, guesses)
  guess = gets.chomp.to_i
  guesses << guess

  if guess == true_num
    puts "That is correct!"
    puts "My number was #{true_num}"
    puts "It took you #{guesses.length} guesses"
    p "You made the following guesses: #{guesses}"

  elsif guess < true_num
    puts "your guess was: #{guess}"
    puts "too low"
    play(true_num, guesses)
  else
    puts "your guess was: #{guess}"
    puts "too high"
    play(true_num, guesses)
  end

end

def line_shuffler
  puts "Please enter a filename."
  file_name = gets.chomp

  lines = File.readlines(file_name)

  shuffled_lines = lines.shuffle

  File.open("#{file_name}-shuffled.txt", "w") do |file|
    shuffled_lines.each do |line|
      puts line
    end
  end

end
